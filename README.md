# Recipe app api proxy

NGINX proxy app for our recipe api

## USAGE

### Environment variables

* `LISTEN_PORT` - Port to listen to (default: `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward request to (default: `9000`)